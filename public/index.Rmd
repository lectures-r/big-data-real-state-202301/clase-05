---
pagetitle: "Clase 05: Prepocesamiento de un conjunto de datos (II)"
title: "**Big Data and Machine Learning en el Mercado Inmobiliario**"
subtitle: "**Clase 05:** Prepocesamiento de un conjunto de datos (II)"
author: 
  name: Eduard Fernando Martínez González
  affiliation: Universidad de los Andes | [BD-ML](https://ignaciomsarmiento.github.io/teaching/BDML)
output: 
  html_document:
    theme: flatly
    highlight: haddock
    toc: yes
    toc_depth: 4
    toc_float: yes
    keep_md: false
    keep_tex: false ## Change to true if want keep intermediate .tex file
    ## For multi-col environments
  pdf_document:
    latex_engine: xelatex
    toc: true
    dev: cairo_pdf
    includes:
      in_header: tex/preamble.tex ## For multi-col environments
    pandoc_args:
        --template=tex/mytemplate.tex ## For affiliation field. See: https://bit.ly/2T191uZ
always_allow_html: true
urlcolor: blue
mainfont: cochineal
sansfont: Fira Sans
monofont: Fira Code ## Although, see: https://tex.stackexchange.com/q/294362
## Automatically knit to both formats
---

```{r setup, include=F , cache=F}
# load packages
require(pacman)
p_load(here,knitr,tidyverse,ggthemes,fontawesome,kableExtra)
p_load(tidyverse,rio,viridis,sf, leaflet, tmaptools)

# option html
options(htmltools.dir.version = F)
opts_chunk$set(fig.align="center", fig.height=4 , dpi=300 , cache=F)
```

<!---------------------->
<!---------------------->
## **[1.] Imágenes satelitales**

### **1.1 Motivación**

#### **Night lights and economic growth**

**Asian Financial Crisis: Java, Indonesia**

![Tomado de: Measuring Economic Growth from Outer Space](pics/Indonesia.png){width=60%}

**Genocide Event: Rwanda**

![Tomado de: Measuring Economic Growth from Outer Space](pics/Rwanda.png){width=70%}

Puede acceder al documento aquí: [Measuring Economic Growth from Outer Space](https://www.aeaweb.org/articles?id=10.1257/aer.102.2.994) 

#### **Night lights and Covid-19**

![](pics/covid.png){width=70%}

Puede acceder al documento aquí: [COVID19 y actividad económica: demanda de energía y luminosidad ante la emergencia](https://economia.uniandes.edu.co/sites/default/files/observatorio/Resultados-luminosidad.pdf)

![](pics/covid_india.jpg){width=50%}

Puede acceder al documento aquí: [Examining the economic impact of COVID-19 in India through daily electricity consumption and nighttime light intensity](https://www.sciencedirect.com/science/article/pii/S0305750X20304149?casa_token=VjcoqRnPQo8AAAAA:DXanp0b9C9w6BvR9C3NF0WfEvWO7uB_DvGBBxC8b8d59l2QVcAdRLGKLf4e-UhGr6HRECXNvxw)

#### **Measuring the size and growth of cities using nighttime light**

Puede acceder al documento aquí: [Measuring the size and growth of cities using nighttime light](https://www.sciencedirect.com/science/article/pii/S0094119020300255?casa_token=cMiibzNrYTEAAAAA:YqwYomGC9QgC81nDaYQmQ_z6Fak1WQgtW6JwobSzTOQCfVsqCzprUGK77XEphlO60A0-eWpPCg) 

#### **Housing Vacancy Rate (VHR) y Luces Nocturnas**

![](pics/hvr.png){width=50%}

Puede acceder al documento aquí: [An estimation of housing vacancy rate using NPP-VIIRS night-time light data and OpenStreetMap data](https://www.tandfonline.com/doi/abs/10.1080/01431161.2019.1615655)

#### **CAUSES OF SPRAWL: A PORTRAIT FROM SPACE**

**Urban Land in Miami, FL**

![Tomado de: CAUSES OF SPRAWL: A PORTRAIT FROM SPACE](pics/urban_land_miami.png){width=50%}

Puede acceder al documento aquí: [CAUSES OF SPRAWL: A PORTRAIT FROM SPACE](https://diegopuga.org/papers/Burchfield_Overman_Puga_Turner_QJE_2006.pdf) 

### **1.2 ¿Datos raster?**

#### **¿Qué es un raster?**

![Tomado de: https://www.neonscience.org](pics/raster.png){width=60%}

#### **Resolución**

![Tomado de: https://www.neonscience.org](pics/rasterize.png){width=60%}

#### **Bandas de un raster**

![Tomado de: https://www.neonscience.org](pics/rgb_raster.png){width=60%}

### **1.3 Fuentes:**

#### **1.3.1 Night lights**

**DMSPL (1992-2013)**

Puede acceder a los datos anuales aquí [1992-2103](https://www.ngdc.noaa.gov/eog/dmsp/downloadV4composites.html) 

**VIIRS (2012-Currently)**

+ Puede acceder a la web del proyecto aquí: [link](https://eogdata.mines.edu/products/vnl/)

+ Puede acceder a los datos mensuales aquí: [link](https://eogdata.mines.edu/nighttime_light/monthly/v10/) 

+ Puede acceder a los datos diarios aquí: [link](https://ngdc.noaa.gov/eog/viirs/download_ut_mos.html) 

**Harmonized (1992-2020)**

Puede acceder a los datos y al paper aquí: [link](https://figshare.com/articles/dataset/Harmonization_of_DMSP_and_VIIRS_nighttime_light_data_from_1992-2018_at_the_global_scale/9828827/2) 

#### **1.3.2 Land cover**

+ Puede acceder a la web del proyecto aquí: [link](https://lcviewer.vito.be/about#available-maps)

+ Puede acceder a los datos aquí: [link](https://land.copernicus.eu/global/products/lc)

+ Puede acceder a la documentación del proyecto aquí: [link](https://zenodo.org/record/4723921#.YZFYNS-B1hE") ## Dictionary

#### **1.3.3 NASA**

Puede acceder a datos de lluvias, temperatura, contaminación, vientos y otros en la página oficial del proyecto GES DISC de la NASA aquí: [https://disc.gsfc.nasa.gov](https://disc.gsfc.nasa.gov).

<!---------------------->
<!---------------------->
## **[2.] Manipular datos raster en R**

<!---------------------->
### **0.0 Configuración inicial**

Para replicar las secciones de esta clase, primero debe descargar el siguiente [proyecto de R](https://gitlab.com/lectures-r/big-data-real-state-202301/clase-05/-/archive/main/clase-05-main.zip?path=clase-05) y abrir el archivo `clase-05.Rproj`. 
 
<!---------------->
#### **Instalar/llamar las librerías de la clase**

```{r , eval=T}
## Instalar/llamar las librerías de la clase
require(pacman) 
p_load(tidyverse,rio,skimr,viridis,osmdata,
       raster,stars, ## datos raster
       ggmap, ## get_stamenmap
       spatstat, ## analis de clusters
       sf, ## Leer/escribir/manipular datos espaciales
       leaflet) ## Visualizaciones dinámicas

## solucionar conflictos de funciones
select = dplyr::select
```

### **2.1 Leer un raster**

```{r , eval=T}
methods(class = "stars")

## importar raster de luces: raster
luces_r = raster('input/raster/night_light_202301.tif')
luces_r

## importar raster de luces: stars
luces_s = read_stars("input/raster/night_light_202301.tif")
luces_s
```

### **2.2 Atributos de un raster**

```{r , eval=T}
## resolucion
0.00416667*110000 

## geometria
st_bbox(luces_s)
st_crs(luces_s)
st_dimensions(luces_s)

## atributos
names(luces_s)
names(luces_s) = "date_202301"

## valores del raster
luces_s[[1]] %>% max(na.rm = T)
luces_s[[1]] %>% min(na.rm = T)
luces_s[[1]] %>% as.vector() %>% summary() 
luces_s[[1]][is.na(luces_s[[1]])==T] %>% head()# Reemplazar NA's
luces_s[[1]][2000:2010,2000:2010]
luces_s[[1]][2000:2010,2000:2010] %>% table() # Sustraer una parte de la matriz

## puedo reproyectar un raster?
st_crs(luces_s)
luces_new_crs = st_transform(luces_s,crs=4126)
luces_s[[1]][2000:2010,2000:2010] # no se alteran las geometrias
luces_new_crs[[1]][2000:2010,2000:2010] # no se alteran las geometrias

## plot data
plot(luces_s)
```

### **2.3 Filtrar usando la gemoetría**

```{r,eval=T,echo=T,warning=T,fig.height=1.5,fig.width=2.5}
## download boundary
quilla <- getbb(place_name = "Barranquilla, Colombia", 
                featuretype = "boundary:administrative", 
                format_out = "sf_polygon") %>% .[1,]

leaflet() %>%
addTiles() %>%
addPolygons(data=quilla)

## cliping
l_quilla_1 = st_crop(x = luces_s , y = quilla) # crop luces de Colombia con polygono de Medellin

l_quilla_1[[1]] %>% t()
```
```{r,eval=T,echo=T}
## Plot data
ggplot() + geom_stars(data=l_quilla_1 , aes(y=y,x=x,fill=date_202301)) + # plot raster
scale_fill_viridis(option="A" , na.value='white') +
geom_sf(data=quilla , fill=NA , col="green") + theme_bw() 
```

<!---------------------->
<!---------------------->
## **[3.] Aplicación: Actividad económica en Barranquilla**

### **3.1 Importar datos 201301**

```{r , eval=T}
## load data
l_quilla_0 = read_stars("input/raster/night_light_201301.tif") %>% st_crop(quilla)
names(l_quilla_0) = "date_201301"

ggplot() + geom_stars(data=l_quilla_0 , aes(y=y,x=x,fill=date_201301)) + # plot raster
scale_fill_viridis(option="A" , na.value='white') +
geom_sf(data=quilla , fill=NA , col="green") + theme_bw() 

## stack rasters: adicionar bandas
l_quilla = c(l_quilla_0,l_quilla_1)
l_quilla
```

### **3.2 Raster a datos vectoriales**

```{r , eval=T}
## st_as_sf
puntos_quilla = st_as_sf(x = l_quilla, as_points = T, na.rm = T) # raster to sf (points)
puntos_quilla

poly_quilla = st_as_sf(x = l_quilla, as_points = F, na.rm = T) # raster to sf (polygons)
poly_quilla

## Plot data
ggplot() + 
geom_sf(data = puntos_quilla , aes(col=date_202301))  + 
scale_color_viridis(option="A" , na.value='white') +
geom_sf(data=quilla , fill=NA , col="green") + theme_test() 

ggplot() + 
geom_sf(data = poly_quilla , aes(fill=date_202301),col=NA)  + 
scale_fill_viridis(option="A" , na.value='white') +
geom_sf(data=quilla , fill=NA , col="green") + theme_test()
```

### **3.3 Asignar el valor del pixel a un lugar**

```{r,eval=T,echo=T,warning=T,fig.height=1.5,fig.width=2.5}
## get polygon
puerto <- getbb(place_name = "Puerto de Barranquilla - Sociedad Portuaria, Colombia", 
                featuretype = "barrier:wall", 
                format_out = "sf_polygon")

leaflet() %>%
addTiles() %>%
addPolygons(data=puerto)

## asignar luminosidad
l_puerto = st_join(puerto,poly_quilla)

## variacion promedio 
df = l_puerto
st_geometry(df) = NULL

df %>% 
summarise(pre=mean(date_201301,na.rm=T) , 
          post=mean(date_202301,na.rm=T)) %>%
mutate(ratio=post/pre-1)
```

<!---------------------->
<!---------------------->
## **[4.] Aplicación: Cambio de cobertura de suelo?**

![Tomado de: https://zenodo.org/record/4723921#.Y08mZ-zMJpR](pics/land_cover.png)

```{r,eval=T,echo=T,warning=T,fig.height=1.5,fig.width=2.5}
## read raster
land_cover = c(read_stars("input/raster/discreta_2015.tif"),
               read_stars("input/raster/discreta_2019.tif"))
0.000992063*111000 # resolucion

## read polygon
quilla_buffer <- st_buffer(x = quilla,dist = 2000)

leaflet() %>%
addTiles() %>%
addPolygons(data=quilla_buffer)

quilla_border <- st_difference(x = quilla_buffer , y = quilla)

leaflet() %>%
addTiles() %>%
addPolygons(data=quilla_border)
```

```{r,eval=T}
## cliping raster
quilla_cover = land_cover %>% st_crop(quilla_buffer)
plot(quilla_cover) ## view data

## cliping raster
border = land_cover %>% st_crop(quilla_border)
plot(border) ## view data

## raster to sf
border_sf = st_as_sf(x=border, as_points = F, na.rm = T) 

## cuantos pixeles se urbanizaron?
border_sf = border_sf %>% 
            rename(c_2015=discreta_2015.tif,c_2019=discreta_2019.tif)
border_sf = border_sf %>% 
            mutate(build = ifelse(c_2015!=50 & c_2019==50,1,0))
table(border_sf$build)
```

<!---------------------->
<!---------------------->
## **[5.] Aplicación: Clusteres espaciales**

```{r,eval=T,echo=T,warning=T,fig.height=1.5,fig.width=2.5}
## restaurantes
points <- opq(bbox = getbb("Bogota Colombia")) %>%
          add_osm_feature(key = "amenity", value = "restaurant") %>%
          osmdata_sf() %>% .$osm_points %>% select(osm_id,name) %>% mutate(restaurant=1)

leaflet() %>% addTiles() %>% addCircles(data=points)

## download boundary
city <- getbb(place_name = "Bogota, Colombia", 
        featuretype = "boundary:administrative", 
        format_out = "sf_polygon") %>% .[[2]]

leaflet() %>% addTiles() %>% addPolygons(data=city)
```

```{r,eval=T}
## afine transformations
points = st_transform(points,3857)
city = st_transform(city,3857)

## sf to ppp
points_p <- as.ppp(X = points["restaurant"])
Window(points_p) <- as.owin(city)
plot(points_p)

#--------------#
## summary
summary(points_p) # las unidades son en metros
intensity(points_p)

## reescalar
points_p = rescale(X = points_p , s = 100 , unitname = "100-metros")
unitname(points_p)

summary(points_p)
intensity(points_p)

## estimated standard error for intensity
sqrt(intensity(points_p)/summary(points_p)$window$area)

## plot 
plot(density(points_p,5)) ## plot density
contour(density(points_p,bw.ppl(points_p)), axes = FALSE) ## contour density

#--------------#
## quadrants
points_q = quadratcount(points_p, nx = 4, ny = 6)
points_q

plot(points_q, cex = 2 , col="red")
```

<!----------------->
<!--- Checklist --->
<!----------------->
### **Referencias**

* Lovelace, R., Nowosad, J., & Muenchow, J. (2019). **Geocomputation with R.** [[Ver aquí]](https://geocompr.robinlovelace.net)

  + Cap. 4: Spatial Data Operations
  + Cap. 5: Geometry Operations
  + Cap. 6: Reprojecting geographic data
  + Cap. 11: Statistical learning
  
* Bivand, R. S., Pebesma, E. J., Gómez-Rubio, V., & Pebesma, E. J. (2013). **Applied spatial data analysis with R.** [[Ver aquí]](https://csgillespie.github.io/efficientR/)

  + Cap. 4: Spatial Data Import and Export
  + Cap. 7: Spatial Point Pattern Analysis
  + Cap. 8: Interpolation and Geostatistics



